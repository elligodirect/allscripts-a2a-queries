#standardSQL
WITH EndsMarked AS (
  SELECT
    Gen2PatientID,
    ProblemID,
    Status,
    OnsetDTTM,
    RecordedDTTM,
    CASE
      WHEN Status = LAG(Status) OVER (PARTITION BY Gen2PatientID, ProblemID ORDER BY ProblemID, RecordedDTTM) THEN 0
      ELSE 1
    END AS IS_START,
    CASE
      WHEN Status = LEAD(Status) OVER (PARTITION BY Gen2PatientID, ProblemID ORDER BY ProblemID, RecordedDTTM) THEN 0
      ELSE 1
    END AS IS_END,
    LAG(RecordedDTTM) OVER (PARTITION BY Gen2PatientID, ProblemID ORDER BY ProblemID, RecordedDTTM) AS PreviousRecordedDTTM,
    LEAD(RecordedDTTM) OVER (PARTITION BY Gen2PatientID, ProblemID ORDER BY ProblemID, RecordedDTTM) AS NextRecordedDTTM
  FROM
    `AS_Queries.ALH_ChemoNeuroPain_Patients`),
  GroupsNumbered AS (
  SELECT
    Gen2PatientID,
    ProblemID,
  Status,
    OnsetDTTM,
    RecordedDTTM,
    IS_START,
    IS_END,
    COUNT(CASE
        WHEN IS_START = 1 THEN 1 END) OVER (ORDER BY Gen2PatientID, ProblemID, RecordedDTTM) AS GroupNum,  previousRecordedDTTM,  NextRecordedDTTM  FROM EndsMarked WHERE IS_START=1 OR IS_END=1 ) SELECT Gen2PatientID,  ProblemID,  Status, CASE
    WHEN LAG(GroupNum) OVER (PARTITION BY Gen2PatientID, ProblemID ORDER BY ProblemID, GroupNum) IS NULL THEN COALESCE(MIN(OnsetDTTM), DATE(MIN(RecordedDTTM)))
    ELSE DATE(MIN(RecordedDTTM))
  END AS BeginDate,
  CASE
    WHEN LEAD(GroupNum) OVER (PARTITION BY Gen2PatientID, ProblemID ORDER BY ProblemID, GroupNum) IS NULL THEN DATE("9999-12-31")
    WHEN MAX(RecordedDTTM) > MAX(NextRecordedDTTM) THEN DATE(MAX(RecordedDTTM))
    ELSE DATE(MAX(NextRecordedDTTM))
  END AS EndDate
FROM
  GroupsNumbered
GROUP BY
  Gen2PatientID,
  ProblemID,
  Status,
  GroupNum
ORDER BY
  Gen2PatientID,
  ProblemID,
  BeginDate