#standardSQL
SELECT
  DISTINCT patients.Gen2PatientID
FROM
  `AS_Queries.ALH_ChemoNeuroPain_Encounters` AS encounters
JOIN
  `AS_Queries.ALH_ChemoNeuroPain_Patients` AS patients
ON
  encounters.Gen2PatientID = patients.Gen2PatientID
JOIN
  `AS_Queries.ALH_ChemoNeuroPain_Episodes` AS episodes
ON
  encounters.Gen2PatientID = episodes.Gen2PatientID
JOIN
  `AS_Queries.ALH_ChemoNeuroPain_Inclusion_Medications_list` AS meds
ON
  encounters.Gen2PatientID = meds.Gen2PatientID
WHERE
  (patients.ErrorFlag <> 'y' OR patients.ErrorFlag IS NULL)
  -- AND EXTRACT(YEAR FROM encounters.EncounterDate)year(encounters.EncounterDate) in (2015, 2016, 2017)
  AND encounters.EncounterDate BETWEEN '2015-06-01' AND '2017-05-31'