#standardSQL
  # Save to AS_Queries.ALH_ChemoNeuroPain_Inclusion_Medications_list
WITH EndsMarked AS (
  SELECT
    Gen2PatientID,
    MedID,
    Status,
    Name,
    StartDTTM,
    EndDTTM,
    RecordedDTTM,
    CASE
      WHEN Status = LAG(Status) OVER (PARTITION BY Gen2PatientID, MedID ORDER BY MedID, RecordedDTTM) THEN 0
      ELSE 1
    END AS IS_START,
    CASE
      WHEN Status = LEAD(Status) OVER (PARTITION BY Gen2PatientID, MedID ORDER BY MedID, RecordedDTTM) THEN 0
      ELSE 1
    END AS IS_END,
    LAG(RecordedDTTM, 1, "1900-01-01") OVER (PARTITION BY Gen2PatientID, MedID, RxNorm ORDER BY MedID, RecordedDTTM) AS PreviousRecordedDTTM,
    LEAD(RecordedDTTM, 1, "2020-12-31") OVER (PARTITION BY Gen2PatientID, MedID, RxNorm ORDER BY MedID, RecordedDTTM) AS NextRecordedDTTM
  FROM
    `AS_Queries.ALH_ChemoNeuroPain_Inclusion_Medications` ),
  GroupsNumbered AS (
  SELECT
    Gen2PatientID,
    MedID,
    Status,
    Name,
    StartDTTM,
    EndDTTM,
    RecordedDTTM,
    IS_START,
    IS_END,
    COUNT(CASE
        WHEN IS_START = 1 THEN 1 END) OVER (ORDER BY Gen2PatientID, MedID, RecordedDTTM) AS GroupNum,  PreviousRecordedDTTM,  NextRecordedDTTM  FROM EndsMarked WHERE IS_START=1 OR IS_END=1 ) SELECT Gen2PatientID, MedID, Status, Name, CASE
    WHEN LAG(GroupNum) OVER (PARTITION BY Gen2PatientID, MedID ORDER BY MedID, GroupNum) IS NULL THEN COALESCE(MIN(StartDTTM), MIN(RecordedDTTM))
    ELSE MIN(RecordedDTTM)
  END AS BeginDate,
  CASE
    WHEN LEAD(GroupNum) OVER (PARTITION BY Gen2PatientID, MedID ORDER BY MedID, GroupNum) IS NULL THEN "9999-12-31"
    WHEN MAX(RecordedDTTM) > MAX(NextRecordedDTTM) THEN MAX(RecordedDTTM)
    ELSE MAX(NextRecordedDTTM)
  END AS EndDate
FROM
  GroupsNumbered
GROUP BY
  Gen2PatientID,
  MedID,
  Status,
  Name,
  GroupNum
ORDER BY
  Gen2PatientID,
  MedID,
  BeginDate