SELECT
  Zip3,
  COUNT(DISTINCT pd.Gen2PatientID) AS PatientCount,
  COUNT(DISTINCT Gen2ProviderID) AS ProviderCount
FROM
  `AS_DeID.PatientDemographics*` pd
JOIN
  `AS_Queries.ALH_ChemoNeuroPain_Query_2` q2
ON 
  pd.Gen2PatientID = q2.Gen2PatientID
GROUP BY Zip3
ORDER BY Zip3