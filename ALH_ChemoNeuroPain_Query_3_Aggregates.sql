SELECT
  Zip3,
  COUNT(DISTINCT pd.Gen2PatientID) AS PatientCount,
  COUNT(DISTINCT Gen2ProviderID) AS ProviderCount
FROM
  `AS_DeID.PatientDemographics*` pd
JOIN
  `AS_Queries.ALH_ChemoNeuroPain_Query_3` q3
ON 
  pd.Gen2PatientID = q3.Gen2PatientID
GROUP BY Zip3
ORDER BY Zip3