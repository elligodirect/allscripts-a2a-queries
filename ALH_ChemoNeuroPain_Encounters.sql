#standardSQL
  # Save to AS_Queries.ALH_ChemoNeuroPain_Encounters
SELECT
  encounters.Gen2PatientID,
  encounters.EncounterID,
  encounters.VersionID,
  encounters.Type,
  COALESCE(EncounterDTTM,
    DATE(RecordedDTTM)) AS EncounterDate,
  encounters.AuditDataFlag
  -- We don't have because we haven't joined
  -- provider.SPEC_EXPL
FROM
  `AS_DeID.Encounters*` encounters
JOIN
  `AS_Queries.ALH_ChemoNeuroPain_Episodes` patients
ON
  encounters.Gen2PatientID = patients.Gen2PatientID
JOIN 
  `AS_DeID.Meaningful*` me
ON 
  LOWER(encounters.Type) = LOWER(me.Type)
WHERE
  patients.Status = "Active"
  AND me.Meaningful IS TRUE
  AND COALESCE(EncounterDTTM,
    DATE(RecordedDTTM)) BETWEEN patients.BeginDate
  AND patients.EndDate
  AND EXTRACT(YEAR
  FROM
    COALESCE(EncounterDTTM,
      DATE(RecordedDTTM))) IN (2014,
    2015,
    2016,
    2017)