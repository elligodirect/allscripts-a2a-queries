#standardSQL
# Save to AS_Queries.ALH_ChemoNeuroPain_InclusionDx
SELECT
  DISTINCT p.gen2clientID,
  p.Gen2PatientID,
  p.GenClientID,
  d.DOBYear,
  p.ProblemID,
  p.VersionID,
  p.EncounterID,
  p.Name,
  p.ICD9,
  p.ICD10,
  p.Snomed,
  p.Status,
  p.ErrorFlag,
  p.DiagnosisDTTM,
  p.OnsetDTTM,
  p.ResolvedDTTM,
  p.RecordedDTTM,
  p.AuditDataFlag,
  p.Gen2ProviderID,
  p.GenProviderID
FROM
  `AS_DeID.Problems*` p
LEFT JOIN
  `AS_DeID.PatientDemographics*` d
ON
  p.Gen2PatientID = d.Gen2PatientID
WHERE
  ( ICD10 = "G62.0"
    OR Snomed = "707088000" )
  AND (LOWER(Category) NOT IN ("family",
    "family history of",
    "maternal history of",
    "paternal history of",
    "fraternal history of",
    "sororal history of",
    "son's history of",
    "rule out",
    "considered",
    "discussion of",
    "discussion of risk of",
    "risk of",
    "uncorroborated history of",
    "education about",
    "printed information given for",
    "rule_out",
    "possible",
    "evaluate preoperatively for") OR Category IS NULL)
  AND (p.ErrorFlag <> "Y" OR p.ErrorFlag IS NULL)
  AND EXTRACT(YEAR FROM CURRENT_DATE()) - d.DOBYear BETWEEN 18 AND 80
  AND Status IN ("Active", "Chronic")